 Pseudo-random number generator 
===============================

Author: Michal Mazurek
----------------------

Overview
--------

This is the simplest version of various distributions' number generator. In this version you may choose to distribute numbers for:   

* Normal Distribution,
* Triangle Distribution,
* Exponential Distribution,
* Lognormal Distribution.  

The numbers are plotted in two dimensional perspective, once per each axis respectively. The aim of this simulation is to show the results of generating distributions using the method of reverted cumulative distribution function. 
Therefore, the simulation's steps are as follows:

* prepare uniform distribution between 0 and 1;
* project all numbers on known CDF using bisection method;
* present data on XY scatter plot;
* write XY coordinates to external file with a name of created distribution.

Not only does this app prepare numbers numerically, but also the uniform distribution number generator is completely independent of already prepared methods and therefore uses simple formula: 

**( *x* * *a* + *c* ) mod *m***

where all parametres are assigned statically. 

Nevertheless, it is incapable of reducing any inadequacies of this generator whatsoever. 

Compilation
-----------

In order to compile this app successfully one has to capable of including Java Swing and open-source JFreeChart library: http://www.jfree.org/jfreechart/.
Note: you may alter the simple generator to generate more reliably random numbers.
 