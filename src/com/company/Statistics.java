package com.company;

import java.util.List;

public interface Statistics {
    double function(double x);
    double bisection( double u);
    List dystrybuanta();
    List get_numbers();
    double get_number_of_points();
    double mean_to_parametre();
}
