package com.company;

import java.util.*;

public abstract class Distributions extends SimpleGenerator implements Statistics{

    private static int number_of_points;
    protected static double mean;
    protected static List numbers;


    public Distributions(int c, int a, int m, int number_of_points, double mean) {
        super(c, a, m);
        this.number_of_points = number_of_points;
        this.mean = mean;
    }

    public Distributions() {
        number_of_points = 100;
    }

    public double function(double x){

        return x;
    }



    public double bisection( double u){
        double x;

        double xmin = 0;
        double xmax = 1;
        x = (xmin + xmax) / 2.0;

        while(true){
            if(function(x) - u > 0) xmax = x;
            else if (function(x) - u < 0 ) xmin = x;
            else if (function(x) - u == 0 ) break;

            x = (xmax + xmin) / 2.0;

            if ( xmax - xmin < 1.0e-6) break;
        }
        return x;
    }

    public List dystrybuanta(){

        numbers = new ArrayList();
        numbers.add(0, mean + 0.0001);
        for (int a = 1; a < number_of_points; a++){
            numbers.add(a, bisection(generate((double) numbers.get(a-1))));
        }
        return numbers;
    }

    public List get_numbers(){
        return numbers;
    }

    public double get_number_of_points(){
        return number_of_points;
    }

    public double mean_to_parametre(){
        return mean;
    }
}
