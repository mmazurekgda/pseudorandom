package com.company;


public class LognormalDistribution extends NormalDistribution{

    public LognormalDistribution(int c, int a, int m, int number_of_points, double mean, double stddev) {
        super(c, a, m, number_of_points, mean, stddev);
    }

    public double stddev_to_parametre(){
        return Math.sqrt(Math.log(stddev*stddev/mean*mean + 1));
    }

    @Override
    public double function(double x) {
        return 0.5 + 0.5 * erf( (Math.log(x) - mean_to_parametre()) / Math.sqrt(2) / stddev_to_parametre());
    }

    @Override
    public double mean_to_parametre() {
        return Math.log(mean*mean/Math.sqrt(stddev*stddev + mean*mean));
    }
}
