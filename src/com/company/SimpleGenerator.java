package com.company;

public class SimpleGenerator implements Generator{
    private int c;
    private int a;
    private int m;

    public  SimpleGenerator(){
        c = 0;
        a = 22695477;
        m = 1;
    }

    public SimpleGenerator(int c, int a, int m) {
        this.c = c;
        this.a = a;
        this.m = m;
    }

    public double generate(double x){
        return ( x * a + c ) % m;
    }

}
