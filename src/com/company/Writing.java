package com.company;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

public class Writing {
    public static void write_to_file(String distribution_name, List x, List y) {
        try {
            PrintWriter write = new PrintWriter(distribution_name);
            write.println(distribution_name);
            write.println("X \t Y");
            for(int i = 0; i < x.size(); i++) {
                write.println( x.get(i) + "\t" + y.get(i));
            }
            write.close();
        } catch (FileNotFoundException e){
            System.out.println("No file found!");
        }
    }
}
