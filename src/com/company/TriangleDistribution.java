package com.company;

public class TriangleDistribution extends Distributions{
    public TriangleDistribution(int c, int a, int m, int number_of_points, double mean) {
        super(c, a, m, number_of_points, mean);
    }

    @Override
    public double function(double x) {

        double result = 1;
        if(x > 0 && x <= mean_to_parametre()){
            result =  x*x/(mean_to_parametre());

        }
        if(x > mean_to_parametre() && x < 1){
            result = 1 - (1-x)*(1-x)/(1 - mean_to_parametre());
        }
        //System.out.println(x);
        return result;
    }

    @Override
    public double mean_to_parametre() {
        return mean*3.0 - 1.0;
    }
}
