package com.company;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.util.*;


import javax.swing.*;


public class Chart {
    private JPanel CartPanel;
    private JButton button1;

    private JComboBox comboBox1;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private Map stats;
    private static Distributions distribution_x;
    private static Distributions distribution_y;

    private void createUIComponents(){
        String[] distributions = {"Normal Distribution", "Exponential Distribution", "Triangle Distribution", "Log-normal Distribution"};
        comboBox1 =  new JComboBox(distributions);
        comboBox1.setSelectedIndex(0);
        textField1 = new JTextField();
        textField1.setText("100");
        textField2 = new JTextField();
        textField2.setText("0.5");
        textField3 = new JTextField();
        textField3.setText("0.1");
    }

    public Chart() {
        button1.addComponentListener(new ComponentAdapter() {
        });

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                stats = new HashMap();

                try {
                    testUnfilledBoxesException(textField2.getText());
                    testUnfilledBoxesException(textField1.getText());
                    testUnfilledBoxesException(textField3.getText());
                    stats.put("mean", Double.parseDouble(textField2.getText()));
                    stats.put("number", Integer.parseInt(textField1.getText()));
                    stats.put("stddev", Double.parseDouble(textField3.getText()));

                    switch ((String) comboBox1.getSelectedItem()) {
                        case "Normal Distribution":
                            distribution_x = new NormalDistribution(1, 22695477, 1, (Integer) stats.get("number"), (Double) stats.get("mean"), (Double) stats.get("stddev"));
                            distribution_y = new NormalDistribution(1, 134775813, 1, (Integer) stats.get("number"), (Double) stats.get("mean"), (Double) stats.get("stddev"));
                            break;
                        case "Exponential Distribution":
                            distribution_x = new ExponentialDistribution(1, 22695477, 1, (Integer) stats.get("number"), (Double) stats.get("mean"));
                            distribution_y = new ExponentialDistribution(1, 134775813, 1, (Integer) stats.get("number"), (Double) stats.get("mean"));
                            break;
                        case "Triangle Distribution":
                            distribution_x = new TriangleDistribution(1, 22695477, 1, (Integer) stats.get("number"), (Double) stats.get("mean"));
                            distribution_y = new TriangleDistribution(1, 134775813, 1, (Integer) stats.get("number"), (Double) stats.get("mean"));
                            break;
                        case "Log-normal Distribution":
                            distribution_x = new LognormalDistribution(1, 22695477, 1, (Integer) stats.get("number"), (Double) stats.get("mean"), (Double) stats.get("stddev"));
                            distribution_y = new LognormalDistribution(1, 134775813, 1, (Integer) stats.get("number"), (Double) stats.get("mean"), (Double) stats.get("stddev"));

                            break;
                        default:
                            distribution_x = new NormalDistribution(1, 22695477, 1, (Integer) stats.get("number"), (Double) stats.get("mean"), (Double) stats.get("stddev"));
                            distribution_y = new NormalDistribution(1, 134775813, 1, (Integer) stats.get("number"), (Double) stats.get("mean"), (Double) stats.get("stddev"));
                    }

                    JFreeChart chart = ChartFactory.createScatterPlot(
                            (String) comboBox1.getSelectedItem(), // chart title
                            "X", // x axis label
                            "Y", // y axis label
                            createDataset(), // data  ***-----PROBLEM------***
                            PlotOrientation.VERTICAL,
                            true, // include legend
                            true, // tooltips
                            false // urls
                    );
                    ChartFrame frame = new ChartFrame("First", chart);
                    frame.pack();
                    frame.setVisible(true);

                    Writing write = new Writing();
                    write.write_to_file((String) comboBox1.getSelectedItem(), distribution_x.get_numbers(), distribution_y.get_numbers());
                } catch(UnfilledBoxes e){
                    e.printStackTrace();
                }
            }
        });
        comboBox1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

            }
        });
        comboBox1.addComponentListener(new ComponentAdapter() {
        });
    }

    private static XYDataset createDataset() {
        XYSeriesCollection result = new XYSeriesCollection();
        XYSeries series = new XYSeries("Random");
        for (int i = 0; i < distribution_x.get_number_of_points(); i++) {
            double x = (double) distribution_x.dystrybuanta().get(i);
            double y = (double) distribution_y.dystrybuanta().get(i);
            series.add(x, y);
        }
        result.addSeries(series);
        return result;
    }

    public static void main(String[] args) {

        JFrame frame = new JFrame("Chart");
        frame.setContentPane(new Chart().CartPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }

    public static void testUnfilledBoxesException(String info) throws UnfilledBoxes{

            if ((info).matches("-?\\d+(\\.\\d+)?") == false){
                throw new UnfilledBoxes("You must provide text fields with a number!");
            }

    }


}
