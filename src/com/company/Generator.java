package com.company;

public interface Generator {
    double generate(double x);
}
