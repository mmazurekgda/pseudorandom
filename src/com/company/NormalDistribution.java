package com.company;


public class NormalDistribution extends Distributions {
    protected double stddev;

    public NormalDistribution(int c, int a, int m, int number_of_points, double mean, double stddev) {
        super(c, a, m, number_of_points, mean);
        this.stddev = stddev;
    }

    public static double erf(double z) {
        double t = 1.0 / (1.0 + 0.5 * Math.abs(z));

        double ans = 1 - t * Math.exp( -z*z   -   1.26551223 +
                t * ( 1.00002368 +
                        t * ( 0.37409196 +
                                t * ( 0.09678418 +
                                        t * (-0.18628806 +
                                                t * ( 0.27886807 +
                                                        t * (-1.13520398 +
                                                                t * ( 1.48851587 +
                                                                        t * (-0.82215223 +
                                                                                t * ( 0.17087277))))))))));
        if (z >= 0) return  ans;
        else        return -ans;
    }

    @Override
        public double function(double x) {
             return 0.5 * (1 + erf((x - mean)/stddev/Math.sqrt(2)));
        }
}
