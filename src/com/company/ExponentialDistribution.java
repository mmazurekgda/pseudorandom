package com.company;


public class ExponentialDistribution extends Distributions{


    public ExponentialDistribution(int c, int a, int m, int number_of_points, double mean) {
        super(c, a, m, number_of_points, mean);
    }


    @Override
    public double mean_to_parametre() {
        return 1/mean;
    }

    @Override
    public double function(double x) {
        return 1.0 - Math.exp(-mean_to_parametre()*x);
    }
}
