package com.company;

public class UnfilledBoxes extends Exception{

    public UnfilledBoxes(String message) {
        super(message);
    }
}
